# CHANGELOG
  
> 本文档由[hz-cli](http://techdoc.oa.com/web/huize-cli)自动生成
> Copyright ©2006-2020 慧择保险 版权所有

## 0.0.2 (2020-03-25)


### ✨功能

* package.json ([03235be](http://techdoc.oa.com/AbbyYiFei/my-app-demo/commit/03235be15fd7f1b208041e1ac4da1a9ba0cf8c89))
* this is test-dev1 ([95995b7](http://techdoc.oa.com/AbbyYiFei/my-app-demo/commit/95995b79c8e504bb8208adee659bf2452f67b934))
* 新增test ([7e55a34](http://techdoc.oa.com/AbbyYiFei/my-app-demo/commit/7e55a346625315e140a1057ac8b9b4ea1fe904a1))
* 新增test2 ([e33517f](http://techdoc.oa.com/AbbyYiFei/my-app-demo/commit/e33517f36e1c01b22b91a250fbcfe02f37ca5478))

---

## 0.0.1 (2020-03-25)


### ✨功能

* package.json ([03235be](http://techdoc.oa.com/AbbyYiFei/my-app-demo/commit/03235be15fd7f1b208041e1ac4da1a9ba0cf8c89))
* this is test-dev1 ([95995b7](http://techdoc.oa.com/AbbyYiFei/my-app-demo/commit/95995b79c8e504bb8208adee659bf2452f67b934))
* 新增test ([7e55a34](http://techdoc.oa.com/AbbyYiFei/my-app-demo/commit/7e55a346625315e140a1057ac8b9b4ea1fe904a1))
* 新增test2 ([e33517f](http://techdoc.oa.com/AbbyYiFei/my-app-demo/commit/e33517f36e1c01b22b91a250fbcfe02f37ca5478))

---
